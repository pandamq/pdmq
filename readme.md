# PandaMQ (Beta)

A TS/JS time-based library to process tasks.


##### PDMQ UI (Beta) Download: 
[https://gitlab.com/pandamq/pdmq-ui/-/releases](https://gitlab.com/pandamq/pdmq-ui/-/releases
)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install PDMQ.

```bash
npm install pdmq
```

## Usage 
#### Initial Module
```typescript
/** Basic Example */
const { pandaMQ } = require("pdmq");

const { consumer, client } = await new pandaMQ({
  debug: true,
  redis_url: redis://username:password@127.0.0.1:6379,
}).init();

```

```typescripe
/** ExpressJS Example */
const app = require('express')();
const os = require("os");
const { pandaMQ } = require("pdmq");

app.listen(process.env.PORT, async () => {
  try {
    const { consumer, client } = await new pandaMQ({
      debug: true,
      redis_url: redis://username:password@127.0.0.1:6379,
      consumer_identity: os.hostname()
    }).init();
  } catch (error) {
    console.error('Some went wrong: ', error);
  }
});
```

#### Consumer
```typescript
consumer.taskQueue.subscribe((task?: PDMQTask) => {
  console.info(moment().toISOString(), 'Received task: ', task);
})
```

#### Producer 
```typescript
/** Add time-based task */
await client.addTask({
  task_name: 'Test Task',
  task_trigger_type: 'weekly',
  task_trigger_time: '15:20:30',
  task_trigger_days: [2, 4]
})

/** Add instance task */
await client.addTask({
  task_name: `Test Task`,
  task_message: JSON.stringify({
    param_1: 1,
    param_2: 2
  }),
  task_description: `Run in next second.`,
  task_trigger_type: 'instant'
})
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)