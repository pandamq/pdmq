export enum WorkflowNodeCategories {
  CONDITION = "condition",
  ACTION    = "action",
  TASK      = "task"
}