export enum FieldTypes {
  JSON = 'json',
  STRING = 'string',
  NUMBER = 'number',
  LIST = 'list',
  BOOLEAN = 'boolean'
}