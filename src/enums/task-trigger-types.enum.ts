export enum TaskTriggerTypes {
  ONCE = "once",
  DAILY = "daily",
  WEEKLY = "weekly",
  MONTHLY = "monthly",
  ANNUALLY = "annually",
  NEVER = "never",
  INSTANT = "instant"
}