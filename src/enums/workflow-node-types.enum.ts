export enum WorkflowNodeTypes {
  SEND_HTTP_REQUEST  = 'send http request',
  SEND_EMAIL         = 'send email',
  SET_VARIABLE       = 'set variable',
  START_WORKFLOW     = 'start workflow',
  DELAY_PROGRESS     = 'delay progress',
  IF_SUCCEED         = 'if succeed',
  IF_FAILED          = 'if failed',
  IF_VARIABLE_EQUALS = 'if variable equals',
  IF_NOT_EQUALS      = 'if not equals'
}