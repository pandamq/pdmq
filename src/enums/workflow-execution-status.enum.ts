export enum WorkflowExecutionStatus {
  RUNNING    = 'Running',
  NEVER_RAN  = 'Never Ran',
  COMPLETED  = 'Completed',
  TERMINATED = 'Terminated'
}