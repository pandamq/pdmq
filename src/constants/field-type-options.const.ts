export const FieldTypeOptions = [
  {
    text: "JSON",
    value: "json"
  },
  {
    text: "String",
    value: "string"
  },
  {
    text: "Number",
    value: "number"
  },
  {
    text: "List",
    value: "list"
  }
]