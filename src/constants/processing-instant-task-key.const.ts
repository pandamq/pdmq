import { TaskTriggerTypes } from "../enums/task-trigger-types.enum";

export const PROCESSING_INSTANT_TASK_KEY = `tasks:PROCESSING:${TaskTriggerTypes.INSTANT}`;