import { createClient, RedisClient } from "redis";
import { PDMQInitialOptions } from "../interfaces/initial-options.interface";
import { PDMQTask } from "../interfaces/task.interface";
import * as moment from "moment"; 
import { RedisService } from "../services/common/redis.service";
import { TaskService } from "../services/main/task.service";
import { TaskTriggerTypes } from "../enums/task-trigger-types.enum";
import { PDMQConsumer } from "../interfaces/consumer.interface";

export class PDMQClient {

  private redisClient: RedisClient;
  private redisService: RedisService;
  private taskService: TaskService;

  constructor(
    private InitialOptions: PDMQInitialOptions
  ) {
    this.init();
  }

  private init() {
    if (this.InitialOptions.debug) console.debug("Initial Client")
    this.redisClient = createClient({
      url: this.InitialOptions.redis_url
    });
    
    this.redisClient.on("ready", () => {
      if (this.InitialOptions.debug) console.info("PDMQ Client is ready");
      this.redisService = new RedisService(this.redisClient);
      this.taskService = new TaskService();
    })
  }

  disconnect() {
    return this.redisClient.quit();
  }

  /**
   * Get Execution Result
   */
  async getExecutionResult(execId: string, options?: {
    remove?: boolean
  }): Promise<{
    res    ?: any,
    error  ?: any,
    success : boolean,
    task    : PDMQTask
  } | null> {
    const [executionResString] = await this.redisService.getMulti([`tasks:COMPLETED:${execId}`]);
    if (options?.remove) {
      await this.redisService.delete([`tasks:COMPLETED:${execId}`]);
    }
    if (executionResString)
      return JSON.parse(executionResString)
    return null;
  }

  /**
   * Get instant processing task
   * @return {PDMQTask | null} the processing instant task
   */
  async getProcessingTask(): Promise<PDMQTask[] | null> {
    const [taskString] = await this.redisService.getMulti([`tasks:PROCESSING:${TaskTriggerTypes.INSTANT}`]);
    if (!taskString) return null;
    const task: PDMQTask = JSON.parse(taskString);
    return [task]
  }

  /**
   * List of all consumers
   */
  async getConsumers(): Promise<PDMQConsumer[]> {
    try {
      const keys = await this.redisService.searchKeys("consumer:*:count");
      if (!keys?.length) return [];
      const reply = await this.redisService.getMulti(keys);
      return reply.map(consumerString => JSON.parse(consumerString));
    } catch (error) {
      return [];
    }
  }

  /**
   * Total number of consumer processors
   */
  async getCurrentConsumersCount(): Promise<number> {
    const keys = await this.redisService.searchKeys("consumer:*:count");
    if (!keys?.length) return 0;
    const reply = await this.redisService.getMulti(keys);
    return reply.map(row => JSON.parse(row)).filter(consumer => {
      return moment(consumer.updated_at).isAfter(moment().subtract(5, 's'));
    }).reduce((pre, value) => value.count + pre, 0) || 0;
  }

  /**
   * Add Task
   * 
   * @param task 
   * @returns {PDMQTask} PDMQTask
   */
  async addTask(task: PDMQTask) {
    return this.taskService.addTask(task, this.redisService);
  }

  /**
   * Fetch All Tasks
   * 
   * @returns {PDMQTask[]} PDMQTask[]
   */
  async findAllTasks(): Promise<PDMQTask[]> {
    const tasks: PDMQTask[] = [];
    const keys = await this.redisService.searchKeys(`tasks:*`);

    if (keys.length) {
      const values = await this.redisService.getMulti(keys);
      if (values.length) tasks.push(...values.map(value => JSON.parse(value)));
    }

    return tasks;
  }

  /**
   * List a range of tasks
   * 
   * @param from ISO Datetime String
   * @param to ISO Datetime String
   */
  async findTask(from: string, to: string): Promise<PDMQTask[]> {
    try {
      const format = "YYYY:MM:DD:HH:mm:ss";
      const fromMoment = moment(from, moment.ISO_8601);
      const toMoment = moment(to, moment.ISO_8601);

      from = fromMoment.format(format);
      to = toMoment.format(format);

      let searchKey = 'tasks:', i = 0;
      while (i < from.length) {
        if (from[i] == to[i]) searchKey += from[i];
        else break;
        i++;
      }
      searchKey += '*';
      const tasks = [];
      let keys = await this.redisService.searchKeys(searchKey);

      keys = keys.filter(key => {
        const runAt = key.slice(6, 25);
        const runtime = moment(runAt, format, true);
        return runtime.isBetween(fromMoment, toMoment, 'second');
      });


      if (keys.length) {
        const values = await this.redisService.getMulti(keys);
        if (values.length) tasks.push(...values.map(value => JSON.parse(value)));
      }

      if (this.InitialOptions.debug) console.info("Found Tasks: ", tasks, searchKey);
      return tasks
    } catch (error) {
      if (this.InitialOptions.debug) console.error(error);
      throw error;
    }
  }

  async findInstantTask() {
    try {
      let keys = await this.redisService.searchKeys(`tasks:${TaskTriggerTypes.INSTANT}:*`);
      if (!keys?.length) return [];

      const taskStrings = await this.redisService.getMulti(keys);
      const tasks: PDMQTask[] = taskStrings.map(string => JSON.parse(string));
      return tasks;
    } catch (error) {
      if (this.InitialOptions.debug) console.error(error);
      throw error;
    }
  }

  /**
   * Fetch all stored task
   * 
   * @returns 
   */
  async findStoredTask() {
    try {
      let keys = await this.redisService.searchKeys(`tasks:${TaskTriggerTypes.NEVER}:*`);
      if (!keys?.length) return [];

      const taskStrings = await this.redisService.getMulti(keys);
      const tasks: PDMQTask[] = taskStrings.map(string => JSON.parse(string));
      return tasks;
    } catch (error) {
      if (this.InitialOptions.debug) console.error(error);
      throw error;
    }
  }

  /**
   * Get stored task by id
   * 
   * @param taskId 
   * @returns 
   */
  async findStoredTaskById(taskId: string) {
    try {
      const taskStrings = await this.redisService.getMulti([`tasks:${TaskTriggerTypes.NEVER}:${taskId}`]);
      const tasks: PDMQTask[] = taskStrings.map(string => JSON.parse(string));
      return tasks[0];
    } catch (error) {
      if (this.InitialOptions.debug) console.error(error);
      throw error;
    }
  }

  async updateTask(task: PDMQTask) {
    try {
      await this.deleteTask(task.task_id);
      const newTask = await this.addTask(task);
      return newTask;
    } catch (error) {
      throw error;
    }
  }

  async deleteTask(taskId: string) {
    try {
      const [key] = await this.redisService.searchKeys(`*:${taskId}`, {
        count: 1
      });
      await this.redisService.delete([key]);
    } catch (error) {
      throw error;
    }
  }

  async runStoredTask(storedTaskId: string): Promise<PDMQTask> {
    return this.taskService.runStoredTask(storedTaskId, this.redisService);
  }
}