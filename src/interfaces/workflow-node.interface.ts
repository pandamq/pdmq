import { WorkflowNodeCategories } from "../enums/workflow-node-categories.enum";

export interface WorkflowNode {
  id        ?: number,
  class     ?: string,
  data      ?: {
    type        : string,
    title       : string,
    category    : WorkflowNodeCategories,
    fields     ?: any;
  },
  html      ?: string,
  inputs    ?: any,
  outputs   ?: any,
  name      ?: string,
  pos_x     ?: number,
  pos_y     ?: number,
  typenode  ?: boolean
}