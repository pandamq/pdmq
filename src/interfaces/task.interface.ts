export interface PDMQTask {
  task_id?: string

  task_name: string

  task_description?: string

  /** TODO */
  task_cron_expression?: string

  task_message?: string,

  task_consumer_identity?: string,

  task_trigger_type: 'once' | 'daily' | 'weekly' | 'monthly' | 'annually' | 'never' | 'instant'

  /** ISO date time string */
  task_trigger_once_datetime?: string

  /** eg. '00:00:00' | '10:10:30' | '19:30:30' | '23:59:59' */
  task_trigger_time?: string

  /** eg. [1, 2, 3, 4, 5, 6, 7] */
  task_trigger_days?: number[]

  /** eg. 01 | 15 | 28 */
  task_trigger_date?: number

  /** eg. ['01', '12'] */
  task_trigger_months?: string[]

  /** eg. [2021, /20[0-9]2/] */
  task_trigger_years?: (string | number)[]

  task_fallback_retry_limit?: number

  /** In Seconds */
  task_fallback_retry_duration?: number

  /** In Seconds */
  task_fallback_task_id?: string

  /** Eg.: ISO */
  trigger_next_at?: string

  task_created_at?: string

  task_updated_at?: string

  /** In Seconds */
  task_instant_timeout?: number

  /** Execution ID for logging */
  task_exec_id?: string

  success?: (res?: any) => void
  failed?: (error?: any) => void
}