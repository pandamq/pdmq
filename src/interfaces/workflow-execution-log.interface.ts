import { WorkflowNodeCategories } from "../enums/workflow-node-categories.enum";
import { BaseInterface } from "./base.interface";

export interface PDMQWorkflowExecutionLog extends BaseInterface {
  process_completed_at?: Date

  process_started_at?: Date

  workflow_execution?: string

  node_execution_id?: string

  node_execution_result?: {
    success?: boolean,
    result?: any
  }

  node_execution_context?: any

  node_id?: number

  node_type?: string

  node_category?: WorkflowNodeCategories
}