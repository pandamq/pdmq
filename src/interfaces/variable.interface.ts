import { FieldTypes } from "../enums/field-types.enum";
import { BaseInterface } from "./base.interface";

export interface PDMQVariable extends BaseInterface {
  variable_name ?: string,
  variable_data ?: string,
  variable_type ?: FieldTypes
}