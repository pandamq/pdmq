export interface PDMQWorkflowExecution {
  workflow: string

  workflow_execution_completed_at: Date

  workflow_execution_terminated_at: Date

  workflow_execution_started_at: Date

  workflow_execution_logs: {
    log_id: string,
    success: boolean
  }[]

}