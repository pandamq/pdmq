export interface PDMQConsumer {
  platform: string,
  total_mem: number,
  free_mem: number,
  cpus: string[],
  count: number,
  identity: string,
  instance_id: string,
  updated_at?: string,
  utc_offset?: number
}