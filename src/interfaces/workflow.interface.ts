import { BaseInterface } from "./base.interface";
import { PDMQWorkflowExecution } from "./workflow-execution.interface";

export interface PDMQWorkflow extends BaseInterface {
  workflow_name             ?: string; 
  workflow_description      ?: string;
  workflow_body             ?: any;
  last_execution            ?: PDMQWorkflowExecution;
  workflow_ignore_failure   ?: boolean;
  workflow_bind_api_key     ?: string;
  workflow_entry_point_cron ?: string;
}
