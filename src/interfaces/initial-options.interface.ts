export interface PDMQInitialOptions {
  redis_url: string,
  debug?: boolean,
  client_only?: boolean,
  consumer_identity?: string
}