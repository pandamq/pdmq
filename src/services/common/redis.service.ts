import { RedisClient } from "redis";
import { TaskTriggerTypes } from "../../enums/task-trigger-types.enum";

export class RedisService {
  constructor(
    private _redisClient: RedisClient
  ) {

  }

  public set(key, value: string):Promise<'OK'> {
    return new Promise((res, rej) => {
      this._redisClient?.set(key, value, (error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }

  public setWithIndex(key, value: string, indexName: string, index: number = 0):Promise<any[]> {
    return new Promise((res, rej) => {
      this._redisClient.multi().set(key, value).zadd(indexName, index, key).exec((error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }

  public async zrange(indexName: string, index: number, end:number): Promise<string[]> {
    return new Promise((res, rej) => {
      this._redisClient.zrange(indexName, index, end, (error, reply) => {
        if (error) return rej(error);
        res(reply);
      })
    })
  }

  public async zrangeRemoveMember(indexName: string, key: string): Promise<void> {
    return new Promise((res, rej) => {
      this._redisClient.zrem(indexName, key, (error) => {
        if (error) return rej(error);
        res();
      })
    })
  }

  public async searchKeys(matchWith: string, options?: {
    count: number
  }): Promise<string[]> {
    const scan = (searchKey, cursor): Promise<[string, string[]]> => {
      return new Promise((res, rej) => {
        this._redisClient.scan(cursor, "MATCH", searchKey, (error, reply) => {
          if (error) return rej(error);
          else if (!reply) return res(['0', []]);
          else return res(reply)
        })
      })
    }

    let cursor = null, reply = [], maxLooping = 1000;
    while (cursor != '0' && maxLooping > 0) {
      const [tempCursor, tempReply] = await scan(matchWith, cursor === null ? '0' : cursor);
      cursor = tempCursor;
      reply = reply.concat(tempReply);
      if (options?.count && reply.length >= options.count) {
        return reply.slice(0, options.count);
      }
      maxLooping--;
    }

    return reply;
  }
  
  public getMulti(keys: string[]): Promise<string[]> {
    return new Promise((res, rej) => {
      this._redisClient?.mget(keys, (error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }

  public copy(source: string, destination: string) {
    return new Promise((res, rej) => {
      this._redisClient?.send_command(`COPY ${source} ${destination}`, (error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }

  public deleteWithIndex(key: string, indexName: string): Promise<any> {
    return new Promise((res, rej) => {
      this._redisClient
        .multi()
        .del(key)
        .zrem(indexName, key)
        .exec((error, reply) => {
          if (error) rej(error);
          res(reply);
        })
    })
  }

  public deleteKeyInIndex(key: string, indexName: string): Promise<number> {
    return new Promise((res, rej) => {
      this._redisClient
        .zrem(indexName, key, (error, reply) => {
          if (error) rej(error);
          res(reply);
        })
    })
  }

  public delete(keys: string[]) {
    return new Promise((res, rej) => {
      if (!keys?.length) res(1);
      this._redisClient.del(...keys, (error, reply) => {
        if (error) rej(error);
        res(reply);
      })
    })
  }
}