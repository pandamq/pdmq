import { TaskTriggerTypes } from "../../enums/task-trigger-types.enum";
import { PDMQTask }         from "../../interfaces/task.interface";
import * as moment          from "moment";
import { v1 }               from "uuid";
import { RedisService }     from "../common/redis.service";

export class TaskService {
  constructor() {}

  /**
   * Calculate Next Run Time
   * 
   * @param task
   * @returns {string} YYYY:MM:DD:HH:mm:ss
   */
  getNextRunTime(task: PDMQTask) {
    /** For tasks only run for once */
    if (task.task_trigger_type === TaskTriggerTypes.ONCE) {
      return moment(task.task_trigger_once_datetime).format("YYYY:MM:DD:HH:mm:ss");
    }

    /** For tasks run every day */
    else if (task.task_trigger_type === TaskTriggerTypes.DAILY) {
      const momentTime = moment(moment().format("YYYY-MM-DD ") + task.task_trigger_time, "YYYY-MM-DD HH:mm:ss");
      const valid      = momentTime.isAfter(moment());
      if (valid) 
        return momentTime.format("YYYY:MM:DD:HH:mm:ss");
      else
        return moment().add(1, 'day').format("YYYY:MM:DD:") + task.task_trigger_time;
    }

    /** For tasks run weekly */
    else if (task.task_trigger_type === TaskTriggerTypes.WEEKLY) {
      const ISODay = Number(moment().format('E'));
      const todate = moment().format("YYYY:MM:DD:");
      for (const day of task.task_trigger_days) {

        /** Skip if day already passed */
        if (day < ISODay)
          continue;

        /** Return if today is after ISO day */
        else if (day > ISODay) 
          return moment().add(day - ISODay, 'day').format(`YYYY:MM:DD:${task.task_trigger_time}`);

        /** Return if days are same but time is after now */
        else if (day == ISODay 
                  && moment(todate + task.task_trigger_time, "YYYY:MM:DD:HH:mm:ss", true).isAfter(moment()))
          return moment().format("YYYY:MM:DD:") + task.task_trigger_time;
      }

      /** Return the first day of next week */
      return moment().add(1, 'week').startOf('week').add(task.task_trigger_days[0], 'day')
                     .format(`YYYY:MM:DD:${task.task_trigger_time}`);
    }

    else if (task.task_trigger_type === TaskTriggerTypes.MONTHLY) {
      const momentTime = moment(moment().format(`YYYY-MM-${task.task_trigger_date} ${task.task_trigger_time}`), "YYYY-MM-DD HH:mm:ss");
      const valid = momentTime.isAfter(moment());

      if (valid) 
        return momentTime.format("YYYY:MM:DD:HH:mm:ss");
      else
        return moment().add(1, 'month').format(`YYYY:MM:${task.task_trigger_date}:${task.task_trigger_time}`);
    }

    else if (task.task_trigger_type === TaskTriggerTypes.ANNUALLY) {
      for (const month of task.task_trigger_months) {
        const momentTime = moment(moment().format(`YYYY-${month}-${task.task_trigger_date} ${task.task_trigger_time}`), "YYYY-MM-DD HH:mm:ss");
        const valid      = momentTime.isAfter(moment());

        if (valid) 
          return momentTime.format("YYYY:MM:DD:HH:mm:ss");
      }
      return moment().add(1, 'year')
                     .format(`YYYY:${task.task_trigger_months[0]}:${task.task_trigger_date}:${task.task_trigger_time}`);
    }
  }

  /**
   * Verify Task Is Valid
   * 
   * @param task 
   */
  verifyTask(task: PDMQTask): boolean | string {
    if (task.task_trigger_type === TaskTriggerTypes.ONCE)
      return moment(task.task_trigger_once_datetime).isValid() &&
        moment(task.task_trigger_once_datetime).isAfter() ? true : 'Invalid value in task_trigger_once_datetime';

    else if (task.task_trigger_type === TaskTriggerTypes.INSTANT || task.task_trigger_type === TaskTriggerTypes.NEVER)
      return true;

    else if (!task.task_trigger_time) 
      return 'Attribute task_trigger_time can not be empty';

    else if (!moment(task.task_trigger_time, 'HH:mm:ss', true).isValid()) 
      return 'Invalid value in task_trigger_time';
    
    else if (task.task_trigger_type === TaskTriggerTypes.WEEKLY) 
      return task.task_trigger_days?.length ? true : 'Attribute task_trigger_days cannot be empty';
      
    else if (task.task_trigger_type === TaskTriggerTypes.MONTHLY) 
      return task.task_trigger_date ? true : 'Attribute task_trigger_date cannot be empty';
      
    else if (task.task_trigger_type === TaskTriggerTypes.ANNUALLY) {
      if (!task.task_trigger_date) return 'Attribute task_trigger_date cannot be empty';
      else if (!task.task_trigger_months?.length) return 'Attribute task_trigger_months cannot be empty';
    }
  
    return true;
  }

  /**
   * 
   * @param storedTaskId 
   * @param redisService 
   * @returns {PDMQTask} New Instant Task
   */
  async runStoredTask(storedTaskId: string, redisService: RedisService): Promise<PDMQTask> {
    const [taskString] = await redisService.getMulti([`tasks:${TaskTriggerTypes.NEVER}:${storedTaskId}`]);
    if (!taskString) throw "Stored Task Not Found";
    const task: PDMQTask = JSON.parse(taskString);
    task.task_trigger_type = TaskTriggerTypes.INSTANT;
    const newInstantTask = await this.addTask(task, redisService);
    return newInstantTask;
  }

  /**
   * Add task to queue
   * 
   * @param task PDMQ Task
   * @returns {PDMQTask} New Task
   */
  async addTask(task: PDMQTask, redisService: RedisService): Promise<PDMQTask> {
    const isValidTask = this.verifyTask(task);
    if (typeof isValidTask == 'string') throw isValidTask;
    if (!task.task_id) {
      task.task_id = v1();
      task.task_created_at = moment().toISOString();
    } else {
      task.task_updated_at = moment().toISOString();
    }
    if (task.task_trigger_type === TaskTriggerTypes.NEVER) {
      await redisService.set(`tasks:${task.task_trigger_type}:${task.task_id}`, JSON.stringify(task));
    }
    else if (task.task_trigger_type === TaskTriggerTypes.INSTANT) {
      await redisService.setWithIndex(
        `tasks:${task.task_trigger_type}:${new Date().getTime()}:${task.task_id}`, 
        JSON.stringify(task), 
        TaskTriggerTypes.INSTANT, 
        0
      )
    }
    else {
      const timeKey = this.getNextRunTime(task);
      task.trigger_next_at = moment(timeKey, "YYYY:MM:DD:HH:mm:ss", true).toISOString();
      if (!timeKey) throw `Unexpected error, unable to get next run time for the task`;
      await redisService.set(`tasks:${timeKey}:${task.task_id}`, JSON.stringify(task));
    }

    return task;
  }
}