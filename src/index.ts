import { PDMQInitialOptions } from "./interfaces/initial-options.interface";
import { PDMQClient } from "./client";
import { PDMQConsumer } from "./consumer";

export class pandaMQ {

  public options: PDMQInitialOptions;

  constructor(
    options: PDMQInitialOptions
  ) {
    this.options = options;
  }

  init(): Promise<{
    client: PDMQClient,
    consumer: PDMQConsumer
  }> {
    return new Promise((res, rej) => {
      try {
        let consumer: PDMQConsumer;
        if (!this.options.client_only) consumer = new PDMQConsumer(this.options);
        const client = new PDMQClient(this.options);

        res({
          consumer,
          client
        });
      } catch (error) {
        rej(error);
      }
    })
  }
}