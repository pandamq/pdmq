import { v1 } from 'uuid';
import { TaskService } from './../services/main/task.service';
import { PDMQTask } from './../interfaces/task.interface';
import { createClient, RedisClient } from "redis";
import { schedule } from "node-cron";
import { CronExpression } from "../enums/cron-expression.enum";
import * as moment from 'moment';
import { PDMQInitialOptions } from "../interfaces/initial-options.interface";
import { Subject } from "rxjs";
import { RedisService } from '../services/common/redis.service';
import { TaskTriggerTypes } from '../enums/task-trigger-types.enum';
import * as os from 'os';
import { PROCESSING_INSTANT_TASK_KEY } from '../constants/processing-instant-task-key.const';

export class PDMQConsumer {

  public  taskQueue        : Subject<PDMQTask> = new Subject();
  private redisClient      : RedisClient;
  private redisService     : RedisService;
  private taskService      : TaskService;
  private consumerIdentity : string;
  private selectedObserver = 0;
  private instanceId       = v1();

  constructor(
    private InitialOptions: PDMQInitialOptions
  ) {
    this.init();
    this.consumerIdentity = InitialOptions.consumer_identity || this.instanceId;
  }

  /**
   * Initial Client and Consumer
   */
  private init() {
    if (this.InitialOptions.debug) console.debug("Initial Consumer")
    this.redisClient = createClient({
      url: this.InitialOptions.redis_url
    });

    this.redisClient.on("ready", async () => {
      this.redisService = new RedisService(this.redisClient);
      this.taskService = new TaskService();
      /** Lookup any expired time based task */
      await this.lookupExpiredTasks();
      /** Clean consumer stats */
      await this.clean();
      schedule(CronExpression.EVERY_SECOND, () => this.lookup());
    })
  }
  
  /**
   * Disconnect Redis
   * 
   * @returns {void}
   */
  disconnect() {
    return this.redisClient.quit();
  }

  /**
   * Rotate Expired Tasks
   * 
   * @returns {void}
   */
  async lookupExpiredTasks() {
    /** List all tasks */
    const keys: string[] = await this.redisService.searchKeys("tasks:*");
    /** tasks:2021:03:19:20:00:00:1c8cfcf0-885d-11eb-b54d-bf0a5991c053 */
    const expiredKeys = keys.filter(key => {
      const arr = key.split(":");
      if (arr.length != 8) return false;
      return moment(arr.slice(1, 7).join(":"), 'YYYY:MM:DD:HH:mm:ss', true).isBefore(moment());
    });

    if (!expiredKeys.length) return;
    if (this.InitialOptions.debug) console.debug("Recreate expired tasks: ", expiredKeys);

    const taskStrings = await this.redisService.getMulti(expiredKeys);
    for (const taskString of taskStrings) {
      const task:PDMQTask = JSON.parse(taskString);
      await this.taskService.addTask(task, this.redisService);
    };

    await this.redisService.delete(expiredKeys);
  }

  /**
   * Delete All Tasks
   */
  async clean() {
    const keys = await this.redisService.searchKeys("consumer:*:count");
    await this.redisService.delete(keys);
  }

  /**
   * Rotate Task
   * 
   * @param task 
   * @returns {PDMQTask}
   */
  async rotateTask(task: PDMQTask) {
    if (task.task_trigger_type == TaskTriggerTypes.ONCE) return ;
    const nextRunTime = this.taskService.getNextRunTime(task);
    const momentNextRunTime = moment(nextRunTime, 'YYYY:MM:DD:HH:mm:ss', true);
    task.trigger_next_at = momentNextRunTime.toISOString();
    if (momentNextRunTime.isValid() && momentNextRunTime.isAfter(moment())) {
      this.redisService.set(`tasks:${nextRunTime}:${task.task_id}`, JSON.stringify(task));
    }
  }

  /**
   * Run Instant Task
   * 
   * @returns {void}
   */
  private async processInstantTask() {
    if (!this.taskQueue.observers.length) return;
    const [firstInstantKey] = await this.redisService.zrange(TaskTriggerTypes.INSTANT, 0, 1);
    
    const [processingTaskString] = await this.redisService.getMulti([PROCESSING_INSTANT_TASK_KEY]);
    if (processingTaskString) {
      const processingTask: PDMQTask = JSON.parse(processingTaskString);
      /** If Timeout */
      if (processingTask.task_instant_timeout != -1 && 
        moment(processingTask.task_updated_at).add(processingTask.task_instant_timeout || 30, 'seconds').isBefore(moment())) {
          await this.redisService.delete([PROCESSING_INSTANT_TASK_KEY]);
          return this.processInstantTask();
      } 
    }
    if (!firstInstantKey || processingTaskString)  return ;

    const [taskString] = await this.redisService.getMulti([firstInstantKey]);
    
    if (!taskString) {
      /** Remove zrange */
      if (firstInstantKey) {
        await this.redisService.zrangeRemoveMember(TaskTriggerTypes.INSTANT, firstInstantKey);
      }
      return;
    }
    const task: PDMQTask = JSON.parse(taskString);

    // Do nothing if the task assigned to another consumer 
    if (task.task_consumer_identity && this.consumerIdentity != task.task_consumer_identity) return;

    // Catch the task, prevent duplicates from other instances
    // Only 1 instance can successfully delete the record
    const getTaskSuccess = await this.redisService.delete([firstInstantKey]);

    if (!getTaskSuccess) return /** Other consumer caught the task */
    if (this.InitialOptions.debug) console.debug('Catch Task Success: ', task.task_name);

    // Also delete key in index
    await this.redisService.deleteKeyInIndex(firstInstantKey, TaskTriggerTypes.INSTANT);

    /** Set Task */
    task.task_updated_at = moment().toISOString();;
    await this.redisService.set(PROCESSING_INSTANT_TASK_KEY, JSON.stringify(task));
    this.taskQueue.observers[this.selectedObserver]?.next(this.formatTaskCallback(task));
  }

  /**
   * Log Consumer
   */
  private logConsumer() {
    this.redisService.set(`consumer:${this.instanceId}:count`, JSON.stringify({
      utc_offset: new Date().getTimezoneOffset() / -60,
      platform: os.platform(),
      total_mem: os.totalmem(),
      free_mem: os.freemem(),
      cpus: os.cpus().map(cpu => cpu.model),
      count: this.taskQueue.observers.length,
      identity: this.consumerIdentity,
      instance_id: this.instanceId,
      updated_at: moment().toISOString()
    }));
  }

  /**
   * Lookup Tasks
   * 
   * @returns {void}
   */
  private async lookup() {
    this.logConsumer();

    /** No Subscriber, retry later */
    if (!this.taskQueue.observers.length) 
      return this.InitialOptions.debug && console.error("No subscribed consumer found.");

    this.processInstantTask();

    // Get Redis Keys
    const timeKey = moment().format("YYYY:MM:DD:HH:mm:ss"), key = `tasks:${timeKey}:*`;
    const keys = await this.redisService.searchKeys(key);
    if (!keys.length) return;
    
    // Get Tasks
    const taskStrings = await this.redisService.getMulti(keys);
    if (!taskStrings.length) return;
    const tasks: PDMQTask[] = taskStrings.map(taskString => JSON.parse(taskString));

    // Log Consumer Status
    if (this.InitialOptions.debug) 
      console.debug('Tasks to process: ', tasks.map(task => task.task_name), `Observers = ${this.taskQueue.observers.length}`);

    // Loop Through Each Task
    for (const task of tasks) {
      // Catch the task, prevent duplicates from other instances
      // Only 1 instance can successfully delete the record
      const getTaskSuccess = await this.redisService.delete([`tasks:${timeKey}:${task.task_id}`]);
      if (!getTaskSuccess) continue;
      if (this.InitialOptions.debug) console.debug('Catch Task Success: ', task.task_name);

      // Pick an observer
      if (this.selectedObserver + 1 >= this.taskQueue.observers.length) this.selectedObserver = 0;
      else this.selectedObserver++;

      this.taskQueue.observers[this.selectedObserver]?.next(this.formatTaskCallback(task));
      this.rotateTask(task);
    }
  }

  /**
   * Handle Task Failure
   * 
   * @param task PDMQ Task
   */
  async handleTaskFailure(task: PDMQTask) {
    try {
      const sourceTask: PDMQTask = JSON.parse(JSON.stringify(task));
      /** Create new task id */
      sourceTask.task_id = v1();
      
      /** verify task retry limit */
      if (typeof sourceTask.task_fallback_retry_limit == 'number' && sourceTask.task_fallback_retry_limit > 0) {
        // reduce limit
        sourceTask.task_fallback_retry_limit -= 1;

        /** add to time based queue */
        if (moment(sourceTask.trigger_next_at).isValid() 
          && typeof sourceTask.task_fallback_retry_duration == 'number' 
          && sourceTask.task_fallback_retry_duration > 0 ) {
          /** calculate next runtime */
          sourceTask.task_trigger_type = TaskTriggerTypes.ONCE;
          sourceTask.task_trigger_once_datetime = moment().add(sourceTask.task_fallback_retry_duration, 's').toISOString();
          await this.addTask(sourceTask);
        }
        /** add to instant queue */
        else {
          sourceTask.task_trigger_type = TaskTriggerTypes.INSTANT;
          await this.addTask(sourceTask);
        }
      } 
      /** run backup task if exist */
      else if (task.task_fallback_task_id) {
        /** Add stored task to instant queue */
        await this.taskService.runStoredTask(task.task_fallback_task_id, this.redisService);
      }
    } catch (error) {
      console.error(`[PDMQ] Unable handle task (${task.task_name} - ${task.task_id}) failure: `, error);
    }
  }

  /**
   * Add Task
   * 
   * @param task 
   * @returns {PDMQTask}
   */
  private addTask(task: PDMQTask) {
    return this.taskService.addTask(task, this.redisService);
  }

  /**
   * 
   * Format task callback
   * 
   * @param task 
   * @returns 
   */
  private formatTaskCallback(task: PDMQTask) {
    task.success = (res) => {
      /** Remove processing key if instant task */
      if (task.task_trigger_type === TaskTriggerTypes.INSTANT)
        this.redisService.delete([PROCESSING_INSTANT_TASK_KEY]);

      /** Store result into redis and await for pdmq-ui to pickup */
      if (task.task_exec_id) {
        this.redisService.set(`tasks:COMPLETED:${task.task_exec_id}`, JSON.stringify({
          success: true,
          res,
          task
        }))
      }

    }
    task.failed = (error) => {
      /** Remove processing key if instant task */
      if (task.task_trigger_type === TaskTriggerTypes.INSTANT)
        this.redisService.delete([PROCESSING_INSTANT_TASK_KEY]);

      /** Store result into redis and await for pdmq-ui to pickup */
      if (task.task_exec_id) {
        this.redisService.set(`tasks:COMPLETED:${task.task_exec_id}`, JSON.stringify({
          success: false,
          error,
          task
        }))
      }

      /** Handle Failure */
      this.handleTaskFailure(task);
    }
    return task;
  }
}
